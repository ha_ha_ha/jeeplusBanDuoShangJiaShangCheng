package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TArticle;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITArticleService extends IService<TArticle> {
	
}
