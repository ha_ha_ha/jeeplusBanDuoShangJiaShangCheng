package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TFloor;
import com.stylefeng.guns.persistence.shop.dao.TFloorMapper;
import com.stylefeng.guns.modular.shop.service.ITFloorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品类型表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TFloorServiceImpl extends ServiceImpl<TFloorMapper, TFloor> implements ITFloorService {
	
}
